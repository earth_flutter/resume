import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget Info = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8,),
              child: Text(
                'My Info',
                style: TextStyle( fontWeight: FontWeight.bold),
                ),
            ),
            Container(
              child: Text('Name : Tanapon Chongutaipaisal'),
            ),
            Container(
              child: Text('Age     : 22 Years'),
            ),
            Container(
              child: Text('Email  : 61160030@go.buu.ac.th'),
            ),
            Container(
              child: Text('Skills',style: TextStyle(fontWeight: FontWeight.bold),),
              alignment: Alignment.center,
              
            )
            
          ],
        ),
        ),
        
    ],
  ),
);

      Color color  = Theme.of(context).primaryColor;

      Widget Skill = Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Image.asset(
              'images/java.jpg',
              width: 100,
              height: 100,
              fit:BoxFit.cover
            ),
            Image.asset(
              'images/py.jpg',
              width: 100,
              height: 100,
              fit:BoxFit.cover
            ),
            Image.asset(
              'images/FF.png',
              width: 100,
              height: 100,
              fit:BoxFit.cover
            ),
    ],
  ),
);

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(padding: const EdgeInsets.only(bottom: 8,),
              child: Text(
                'ประวัติส่วนตัว',
                style: TextStyle( fontWeight: FontWeight.bold),
                ),
                ),
                Container(
                  child: Text('ชื่อเล่น : เอิร์ท'),
                ),
                Container(
                  child: Text('เบอร์โทร : 089-9314177'),
                ),
                Container(
                  child: Text('งานอดิเรก : เล่นเกม'),
                ),
                Container(
                  child: Text('จบมัธยมต้นจาก : ตาษตระการคุณ'),
                ),
                Container(
                  child: Text('ตอนนี้ศึกษาที่ : บูรพา'),
                ),
          ],
          )
          ),
        ],
      )
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume'),
          ),
          body: ListView(children: [
            Image.asset(
              'images/me.jpg',
              width: 600,
              height: 240,
              fit:BoxFit.cover
            ),
            Info,
            Skill,
            textSection],),
          )
    );
  }
 
  
}